class human:
    name = "姓名"

    def __init__(self):
        global name
        name = input("请输入姓名：")

    def run(self):
        print("{} is running happily".format(name))

    def eat(self):
        print("{} is going to eat something delicious".format(name))


class chinese(human):
    __secret = "国家机密"

    def speak(self):
        print("{} is speaking chinese".format(name))

    def write(self):
        print("{} is writing chinese charaters".format(name))


class besti(chinese):
    __address__ = "丰台区富丰路7号"

    def principle(self):
        print("{} is loyal,studious,rigorous and disciplined".format(name))


student = besti()
student.run()
student.speak()
student.principle()
try:
    print(student.__secret)
except:
    print("!!secret不可访问!!")
print(student.__address__)
