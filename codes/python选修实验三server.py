import socket
import base64
import binascii
from gmssl import sm2, func

#16进制的公钥和私钥
private_key = '00B9AB0B828FF68872F21A837FC303668428DEA11DCD1B24429D0C99E24EED83D5'
public_key = 'B9C9A6E04E9C91F7BA880429273747D7EF5DDEB0BB2FF6317EB00BEF331A83081A6994B8993F3F5D6EADDDB81872266C87C018FB4162F5AF347B483E24620207'
sm2_crypt = sm2.CryptSM2(
    public_key=public_key, private_key=private_key)

s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
host = socket.gethostname()
port = 12345
s.bind((host,port))
s.listen(5)

new_s,addr = s.accept()
data = new_s.recv(1024)
#print(data)

dec_data =sm2_crypt.decrypt(data)
dec_data = dec_data.decode()
#print(dec_data)

name = input("请填写解密后明文的文件名称")
f = open(name,"w+")
f.write("\n经服务器解密后的明文是:\n")
f.write(dec_data)
f.close()
f = open(name,"r+")
f0 = f.readlines()
f0 = ''.join(f0)
print(f0)
f.close()

new_s.close()
