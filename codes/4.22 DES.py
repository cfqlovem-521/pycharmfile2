from Crypto.Cipher import DES
import binascii
from docx import Document
path = "D:\\pythonfiles\\文件操作\\明文.docx"
file = Document(path)
for p in file.paragraphs:
    text = p.text
# 这是密钥
key = b'abcdefgh'
# 需要去生成一个DES对象
des = DES.new(key, DES.MODE_ECB)
# 需要加密的数据
text = text + (8 - (len(text) % 8)) * '='
# 加密的过程
encrypto_text = des.encrypt(text.encode())
# 加密过后二进制转化为ASCII
encrypto_text = binascii.b2a_hex(encrypto_text)
Docx = Document()
Docx.add_paragraph(str(encrypto_text))
Docx.save("密文.docx")
#print(encrypto_text)
# 解密需要ASCII 先转化为二进制 然后再进行解密
plaint = des.decrypt(binascii.a2b_hex(encrypto_text))
plaint = str(plaint)[1:].strip('\'')
plaint = plaint.strip('=')
Docx = Document()
Docx.add_paragraph(plaint)
Docx.save("解密文件.docx")
