import xlwt
import datetime
import RandomName as r
workbook = xlwt.Workbook(encoding = 'ascii')
worksheet = workbook.add_sheet('My Worksheet',cell_overwrite_ok=True)
style = xlwt.XFStyle() # 初始化样式
font = xlwt.Font() # 为样式创建字体
font.name = 'Times New Roman'
font.bold = True # 黑体
font.underline = True # 下划线
font.italic = True # 斜体字
style.font = font # 设定样式
for i in range(1,50):
    name = r.random_name().split('\t')[0]
    sex  = r.random_name().split('\t')[1]
    worksheet.write(i, 0,  name) # 不带样式的写入
    worksheet.write(i, 1, sex)

workbook.save('20191206陈发强.xls') # 保存文件
