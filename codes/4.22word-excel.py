import xlwt
import datetime
import RandomName as r
from docx import Document
path = "D:\\pythonfiles\\文件操作\\彩蛋4.docx."
file = Document(path)
table = []
for p in file.paragraphs:
    text = p.text
    text = text.split('\t')
    table.append(text)
rows = len(table)-1
items = len(table[0])

workbook = xlwt.Workbook(encoding = 'ascii')
worksheet = workbook.add_sheet('My Worksheet',cell_overwrite_ok=True)
style = xlwt.XFStyle() # 初始化样式
font = xlwt.Font() # 为样式创建字体
font.name = 'Times New Roman'
font.bold = True # 黑体
font.underline = True # 下划线
font.italic = True # 斜体字
style.font = font # 设定样式
for i in range(rows):
    for j in range(items):
        worksheet.write(i, j,table[i][j]) # 不带样式的写入

workbook.save('彩蛋4.xls') # 保存文件
