import pymysql
db = pymysql.connect(host = "localhost" ,port = 3306 ,user = "root",password = "Cfq_13238853277",database = "python20200429")

#创建数据库，连接对象

#游标对象
cursor = db.cursor()
#创建一个表
cursor.execute('SELECT VERSION()')
data = cursor.fetchone()
print('database version:%s' % data)

cursor.execute('drop table if exists cfq20191206')

sql1 = """CREATE TABLE `cfq20191206` (
  `first_name` varchar(255) DEFAULT NULL COMMENT '姓',
  `last_name` varchar(255) DEFAULT NULL COMMENT '名',
  `age` int(11) DEFAULT NULL COMMENT '年龄',
  `sex` varchar(255) DEFAULT NULL COMMENT '性别',
  `income` varchar(255) DEFAULT NULL COMMENT '收入'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
"""
cursor.execute(sql1)
sql21 = """INSERT INTO cfq20191206(first_name,
         last_name, age, sex, income)
         VALUES ('赵', '丽颖', 38, '女', 15000)"""

sql22 = """INSERT INTO cfq20191206(first_name,
         last_name, age, sex, income)
         VALUES ('陈', '发强', 19, '男', 100)"""

sql23 = """INSERT INTO cfq20191206(first_name,
         last_name, age, sex, income)
         VALUES ('李', '太白', 35, '男', 99999)"""

sql24 = """INSERT INTO cfq20191206(first_name,
         last_name, age, sex, income)
         VALUES ('鱼', '玄机', 21, '女', 555)"""

cursor.execute(sql21)
cursor.execute(sql22)
cursor.execute(sql23)
cursor.execute(sql24)

sql3 = "SELECT * FROM cfq20191206"
cursor.execute(sql3)
results = cursor.fetchall()
print(results)

sql4 = """UPDATE cfq20191206 SET age=5+age WHERE sex = '女'"""
cursor.execute(sql4)
sql3 = "SELECT * FROM cfq20191206"
cursor.execute(sql3)
results = cursor.fetchall()
print(results)

sql5 = 'DELETE FROM cfq20191206 WHERE income > 5000 AND age >30'
cursor.execute(sql5)
sql3 = "SELECT * FROM cfq20191206"
cursor.execute(sql3)
results = cursor.fetchall()
print(results)

db.close()
