from gmssl import sm2, func
import socket
private_key = '00B9AB0B828FF68872F21A837FC303668428DEA11DCD1B24429D0C99E24EED83D5'
public_key = 'B9C9A6E04E9C91F7BA880429273747D7EF5DDEB0BB2FF6317EB00BEF331A83081A6994B8993F3F5D6EADDDB81872266C87C018FB4162F5AF347B483E24620207'
sm2_crypt = sm2.CryptSM2(
    public_key=public_key, private_key=private_key)


s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
host = socket.gethostname()
port = 12345
s.bind((host,port))
s.listen(5)

conn,addr = s.accept()
enc_data = conn.recv(1024)
print("服务器发来的密文是：\n{}".format(enc_data))
dec_data =sm2_crypt.decrypt(enc_data)
dec_data = dec_data.decode()
print("解密后的明文是：\n{}".format(dec_data))
conn.close()
s.close()
