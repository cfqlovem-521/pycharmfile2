import socket
import threading

# 服务器端的socket初始化
c = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = socket.gethostname()
# print(host)
port = 12345
c.connect((host, port))  # 元组形式 （IP地址，端口）
while True:
    try:
        str1 = input("请输入要传输的内容")
        c.sendall(str1.encode())
        data = c.recv(1024)
        print(data.decode())
    except:
        break
c.close()

