import socket

# 客户端的socket初始化
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = socket.gethostname()
# print(host)
port = 12345
s.bind((host, port))
s.listen(5)

while True:
    try:
        conn, address = s.accept()
        data = conn.recv(1024)
        print(data.decode())
        conn.sendall(("服务器已经接收到了数据内容：" + str(data.decode())).encode())
    except:
        print("异常")
conn.close()




