preclaim = '''
使用说明：
查询角色的职业请键入：     find_position()
查询职业有哪些角色请键入： show_roles()
给某职业添加新角色请键入： add_new()
更改角色的职业请键入：     position_transfer()

'''
print("{:^}".format(preclaim))

tank = ['夏侯淳', '亚瑟', '程咬金']
assassin = ['阿轲', '兰陵王', '李白']
achor = ['后羿', '孙尚香', '鲁班']
master = ['貂蝉', '小乔', '妲己']


def show_roles():
    a = eval(input("请输入要查询的职业："))
    for i in a:
        print(i, end=',')
    print('\n')
    return True


def add_new():
    role = input("请输入角色的名称：")
    position = eval(input("请输入角色的职业："))
    position.append(role)
    print(position)
    return True


def position_transfer():
    role = input("请输入角色的名称：")
    strold = input("请输入角色原来的职业：")
    strnew = input("请输入角色现在的职业：")
    old = eval(strold)
    new = eval(strnew)
    old.remove(role)
    new.append(role)
    print(strold, old)
    print(strnew, new)
    return True


def find_position():
    role = input("请输入角色的名称：")
    if role in tank:
        print("tank")
    elif role in assassin:
        print("assassin")
    elif role in achor:
        print("achor")
    elif role in master:
        print("master")
    else:
        print("无此角色")
    return True


while True:
    if eval(input("欢迎使用the program made by cfq\n")) == True:
        continue
    else:
        break



