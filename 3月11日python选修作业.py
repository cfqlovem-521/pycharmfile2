front = "决策树：判断一个学生是好学生还是坏学生\n从考试分数、出勤率、作业上交率三个方面"
print("{:*^100}\n".format(front))


def mark():
    a = eval(input('请输入学生的分数'))
    if a >= 90:
        print("是好学生")
    elif a < 60:
        print("不是好学生")
    else:
        return 0


def attendrate():
    a = eval(input('请输入学生的出勤率（小数形式）'))
    if a >= 0.9 and a <= 1:
        print("是好学生")
    elif a < 0.6:
        print("不是好学生")
    elif a > 1:
        print("!输入数据有误")
    else:
        return 0


def homeworkrate():
    a = eval(input("请输入学生的作业上交率（小数形式）"))
    if a >= 0.95 and a <= 1:
        print('是好学生')
    elif a > 1:
        print("!输入数据有误")
    else:
        print("不是好学生")


def main():
    if mark() == 0:
        if attendrate() == 0:
            homeworkrate()
        else:
            pass
    else:
        pass


main()